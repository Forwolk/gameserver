package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import security.HashGenerator;

import java.util.HashSet;
import java.util.UUID;

public class SinglePlayerGameSession implements GameSession{
    private String id = HashGenerator.MD5(UUID.randomUUID().toString()).substring(0,7);

    @NotNull
    private final Logger log = LogManager.getLogger(SinglePlayerGameSession.class);

    private HashSet<Player> players = new HashSet<>();

    @Override
    public void join(@NotNull Player player) throws NoFreeSpaceException {
        log.info("Trying to join session " + this);
        if (players.size() >= GameConstants.MAX_PLAYERS_IN_SESSION)
            throw new NoFreeSpaceException("No free space in session");
        players.add(player);
        log.info(String.format("Player %s is joined to session %s.", player.toString(), this));
    }

    public void quit (@NotNull Player player){
        players.remove(player);
    }

    public String toString (){
        return String.format("Session{id = %s}", id);
    }

    public Player[] getLeaderBoard (){
        Player[] top;
        int t = 0;
        if (players.size() >= 10)
            t = 10;
        else
            t = players.size();
        top = new Player[t];
        HashSet<Player> tempSet = (HashSet<Player>) players.clone();
        for (int i = 0; i < t; i++){
            Player maxPlayer = tempSet.iterator().next();
            int maxScore = maxPlayer.getScore();
            for (Player p : tempSet){
                if (p.getScore() > maxScore){
                    maxScore = p.getScore();
                    maxPlayer = p;
                }
            }
            top[i] = maxPlayer;
            tempSet.remove(maxPlayer);
        }
        return top;
    }
}

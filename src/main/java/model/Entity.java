package model;

import com.sun.javafx.geom.Vec2d;
import org.jetbrains.annotations.NotNull;

public abstract class Entity {
    @NotNull
    private String name;
    @NotNull
    private Vec2d position;
    public double radius = -1d;

    public abstract void die();

    @NotNull
    public Vec2d getPosition() {
        return position;
    }

    public void setPosition(@NotNull Vec2d position) {
        this.position = position;
    }

}

package model;

/**
 * Global static constants
 *
 * @author apomosov
 */
public interface GameConstants {
  int MAX_PLAYERS_IN_SESSION = 10;
  int WORLD_HEIGHT = 1000;
  int WORLD_WIDTH = 1000;
}

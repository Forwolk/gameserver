package model;

public class NoFreeSpaceException extends Exception{
    private String msg;

    public NoFreeSpaceException (String msg){
        this.msg = msg;
    }

    public String getMessage() {
        return msg;
    }

}
